import 'package:app_module_3/dashbord_view.dart';
import 'package:flutter/material.dart';


class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Edit Profile"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              const Text("Full Name"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Username"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Email"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Password"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ConstrainedBox(
                    constraints: const BoxConstraints.tightFor(width: 250, height: 60),
                    child: ElevatedButton(
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => const DashboardView()),
                          );
                        },
                        child: const Text("Update",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                        style: ElevatedButton.styleFrom(primary: Colors.pink ,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
