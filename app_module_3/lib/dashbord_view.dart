import 'package:app_module_3/edit_profile_view.dart';
import 'package:flutter/material.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Dashboard"),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints.tightFor(width: 250, height: 60),
              child: ElevatedButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const DashboardView()),
                    );
                  },
                  child: const Text("Feature 1",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                  style: ElevatedButton.styleFrom(primary: Colors.pink ,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
              ),
            ),
            ConstrainedBox(
              constraints: const BoxConstraints.tightFor(width: 250, height: 60),
              child: ElevatedButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const DashboardView()),
                    );
                  },
                  child: const Text("Feature 2",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                  style: ElevatedButton.styleFrom(primary: Colors.pink ,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
              ),
            ),
            ConstrainedBox(
              constraints: const BoxConstraints.tightFor(width: 250, height: 60),
              child: ElevatedButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const ProfileView()),
                    );
                  },
                  child: const Text("Edit Profile",style: TextStyle(color: Colors.white,fontSize: 17.0,fontWeight: FontWeight.bold),),
                  style: ElevatedButton.styleFrom(primary: Colors.pink ,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)))
              ),
            ),
          ],
        ),
      ),
    );
  }
}

